#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables
export CATKIN_TEST_RESULTS_DIR="/home/woolywhale/ros/ros_example/build/test_results"
export ROS_TEST_RESULTS_DIR="/home/woolywhale/ros/ros_example/build/test_results"

# modified environment variables
export CMAKE_PREFIX_PATH="/home/woolywhale/ros/ros_example/build/devel:$CMAKE_PREFIX_PATH"
export CPATH="/home/woolywhale/ros/ros_example/build/devel/include:$CPATH"
export LD_LIBRARY_PATH="/home/woolywhale/ros/ros_example/build/devel/lib:/home/woolywhale/ros/ros_example/build/devel/lib/x86_64-linux-gnu:$LD_LIBRARY_PATH"
export PATH="/home/woolywhale/ros/ros_example/build/devel/bin:$PATH"
export PKG_CONFIG_PATH="/home/woolywhale/ros/ros_example/build/devel/lib/pkgconfig:/home/woolywhale/ros/ros_example/build/devel/lib/x86_64-linux-gnu/pkgconfig:$PKG_CONFIG_PATH"
export PYTHONPATH="/home/woolywhale/ros/ros_example/build/devel/lib/python2.7/dist-packages:$PYTHONPATH"
export ROSLISP_PACKAGE_DIRECTORIES="/home/woolywhale/ros/ros_example/build/devel/share/common-lisp"
export ROS_PACKAGE_PATH="/home/woolywhale/ros/ros_example:/opt/ros/indigo/share:/opt/ros/indigo/stacks"